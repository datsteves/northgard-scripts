function saveState() { }

var customObjectiveMeta = {
	title: "your title",
	summary: "something short i guess?",
	winGameWhenAllObjectives: true,
	loseGameWhenAnyObjectiveIsMissed: true,
	defeatMessage: "You lost due to missing objectives",
	winMessage: "You won i guess",
	lostMessage: "other player won as we know, right?",
}
var customObjectives = [
	{
		id: "getThatFame",
		name: "Get 50 Fame",
		goal: 50,
		missOnGoal: false,
		startStatus: OStatus.Empty,
		getVal: function () {
			var fame = player.getResource(Resource.Fame);
			return fame;
		},
		onComplete: function () {
			// maybe give them a treat?
		},
		onMiss: function () {
			// maybe say you are defeated already?
		}
	},
	{
		id: "getSomeLand",
		name: "get 24 Zones",
		goal: 24,
		missOnGoal: false,
		startStatus: OStatus.Empty,
		getVal: function () {
			var zoneNr = player.zones.length;
			return zoneNr;
		},
		onComplete: function () {
			// maybe give them a treat?
		},
		onMiss: function () {
			// maybe say you are defeated already?
		}
	},
	{
		id: "winUntil50Years",
		name: "Win within 50 years",
		goal: 50,
		missOnGoal: true,
		startStatus: OStatus.Done,
		getVal: function () {
			var currentYear = toInt(state.time / YEAR);
			return currentYear;
		},
		onComplete: function () {

		},
		onMiss: function () {

		}
	}
];

// --- Script code ---
function init() {
	if (state.time == 0) {
		onFirstLaunch();
	}
	onEachLaunch();
}

function onFirstLaunch() {
	player.addResource(Resource.Wood, 100);
}

function onEachLaunch() {
	state.objectives.title = customObjectiveMeta.title;
	state.objectives.summary = customObjectiveMeta.summary;
	for(obj in customObjectives) {
		state.objectives.add(obj.id, obj.name, {
			visible: true,
			val: obj.getVal(),
			status: obj.startStatus,
			showProgressBar: true,
			showOtherPlayers: true,
			goalVal: obj.goal,
		});
	}
	player.addResource(Resource.Wood, 10);
}

function calcObjectives() {
	for(obj in customObjectives) {
		var nextValue = obj.getVal();
		state.objectives.setCurrentVal(obj.id, nextValue);
		if(obj.goal >= nextValue && obj.missOnGoal) {
			state.objectives.setStatus(obj.id, OStatus.Missed);
			obj.onMiss();
		}
		if(obj.goal >= nextValue && !obj.missOnGoal) {
			state.objectives.setStatus(obj.id, OStatus.Done);
			obj.onComplete();
		}
	}
	var allObjectivesDone = true;

	for(obj in customObjectives) {
		if(customObjectiveMeta.loseGameWhenAnyObjectiveIsMissed) {
			if(state.objectives.getStatus(obj.id) == OStatus.Missed) {
				customDefeat(customObjectiveMeta.defeatMessage);
			}
			allObjectivesDone = false;
		}
	}
	if(customObjectiveMeta.winGameWhenAllObjectives && allObjectivesDone) {
		player.customVictory(customObjectiveMeta.winMessage, customObjectiveMeta.lostMessage);
	}
	player.addResource(Resource.Wood, 10);
}


function isEventAlreadyScheduled() {
	// get game state for scheduled events, from the game it self or the custom ones
	return false;
}

function isTimeInWinter(time) {
	var notWinterDuration = YEAR - WINTER_DURATION;
	var isWinter = time % YEAR > notWinterDuration;
	return isWinter;
}

function eventLoop() {
	var monthsPast = state.time / MONTH;
	var yearsPast = state.time / YEAR;
	// starts with march. 0 => march, 1 => april, ...
	var monthOfTheYear = (monthsPast) / 12;
	var isWinter = isTimeInWinter(state.time);
	var rand = randomInt(1000);

	if(!isEventAlreadyScheduled()) {
		wait(MONTH);
		eventLoop();
		return;
	}

	// schedule Events here?

	wait(MONTH);
	eventLoop();
}

// Regular update is called every 0.5s
function regularUpdate(dt : Float) {
	// eventLoop();
	// calcObjectives();
}